FROM node
MAINTAINER Jan Macku

WORKDIR /usr/src/app
COPY package*.json ./

RUN npm install

EXPOSE 8080
CMD [ "npm", "start" ]
