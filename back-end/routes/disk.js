const express = require('express');
const router = express.Router();

const shell = require('shelljs');

/* -------------------------------------------------- */

/**
 * Method for mounting disk via shell
 */
router.get('/mount/:disk', (req, res) => {
  const mount = 'mount';
  switch (req.params.disk) {
    
    case 'all':
      shell.exec(`echo "${pass}" >> ../shell-scripts/passphrase.txt\n` +
        `./../shell-scripts/disk.sh ${mount} ${req.params.disk} < ../shell-scripts/passphrase.txt\n` +
        'rm ../shell-scripts/passphrase.txt', {silent:true}, (code, stdout, stderr) => {
        res.send(shellOut(code, stdout, stderr));
      });
      break;

    case 'films':
      shell.exec(`./../shell-scripts/disk.sh ${mount} ${req.para,s.disk}`, {silent:true}, (code, stdout, stderr) => {
        res.send(shellOut(code, stdout, stderr));
      });
      break;
    
    case 'documents':
      shell.exec(`./../shell-scripts/disk.sh ${mount} ${req.para,s.disk}`, {silent:true}, (code, stdout, stderr) => {
        res.send(shellOut(code, stdout, stderr));
      });
      break;
    
    case 'others':
      shell.exec(`echo "${pass}" >> ../shell-scripts/passphrase.txt\n` +
        `./../shell-scripts/disk.sh ${mount} ${req.params.disk} < ../shell-scripts/passphrase.txt\n` +
        'rm ../shell-scripts/passphrase.txt', {silent:true}, (code, stdout, stderr) => {
        res.send(shellOut(code, stdout, stderr));
      });
      break;
   
    default:
    // TODO send msg to front-end
  }
});

/**
 * Method for unmounting disk via shell
 */
router.get('/umount/:disk', (req, res) => {
  const mount = 'umount';
  switch (req.params.disk) {
    
    case 'all':
      shell.exec(`./../shell-scripts/disk.sh ${mount} ${req.params.disk}`, {silent:true}, (code, stdout, stderr) => {
        res.send(shellOut(code, stdout, stderr));
      });
      break;

    case 'films':
      shell.exec(`./../shell-scripts/disk.sh ${mount} ${req.para,s.disk}`, {silent:true}, (code, stdout, stderr) => {
        res.send(shellOut(code, stdout, stderr));
      });
      break;
    
    case 'documents':
      shell.exec(`./../shell-scripts/disk.sh ${mount} ${req.para,s.disk}`, {silent:true}, (code, stdout, stderr) => {
        res.send(shellOut(code, stdout, stderr));
      });
      break;
    
    case 'others':
      shell.exec(`./../shell-scripts/disk.sh ${mount} ${req.params.disk}`, {silent:true}, (code, stdout, stderr) => {
        res.send(shellOut(code, stdout, stderr));
      });
      break;
   
    default:
    // TODO send msg to front-end
  }
});

/* --------------------------------------------------- */

function shellOut(code, stdout, stderr) {
  return {
    exitCode: `Exit code: ${code}`,
    programOut: `Program output: ${stdout}`,
    programErr: `Program stderr: ${stderr}`
  };
}

module.exports = router;
