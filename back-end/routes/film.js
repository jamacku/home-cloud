const express = require('express');
const router = express.Router();

const fs = require('fs');
const pg = require('pg');

const database = require('../database');

/* Setup database */
const pool = new pg.Pool(database.getAccess());

/* -------------------------------------------------- */

/**
 * Method for get list of films in database
 */
router.get('/list', (req, res) => {
  pool.connect((err, client, done) => {
    if(err){
      console.log(database.logConnectionError(err));
      res.status(400).send(err);
    }
    client.query(database.getFilteredListOfFilms(), (err, result) => {
      done();
      if(err){
        console.log(err);
        res.status(400).send(err);
      }
      res.status(200).send(result.rows);
    });
  });
});

/**
 * Method for get filtered list of films in database
 */
router.post('/list/find', (req, res) => {
  /*let bindValues = [];

  Object.values(req.body).forEach((value) => {
    if(value) {bindValues.push(value);}
  });*/

  pool.connect((err, client, done) => {
    if(err){
      console.log(database.logConnectionError(err));
      res.status(400).send(err);
    }
    client.query(database.getFilteredListOfFilms(req.body.name, req.body.year), (err, result) => {
      done();
      if(err){
        console.log(err);
        res.status(400).send(err);
      }
      res.status(200).send(result.rows);
    });
  });
});

/**
 * Method for get film by id
 */
router.get('/list/:filmId', (req, res) => {
  pool.connect((err, client, done) => {
    if(err){
      console.log(database.logConnectionError(err));
      res.status(400).send(err);
    }
    client.query(database.getFilmById(), [req.params.filmId], (err, result) => {
      done();
      if(err){
        console.log(err);
        res.status(400).send(err);
      }
      res.status(200).send(result.rows);
    });
  });
});

/**
 * Method for stream video
 */
router.get('/stream/:filmId', (req, res) => {
  pool.connect((err, client, done) => {
    if(err){
      console.log(database.logConnectionError(err));
      res.status(400).send(err);
    }
    client.query(database.getFilmSource(), [req.params.filmId], (err, result) => {
      done();
      if(err){
        console.log(err);
        res.status(400).send(err);
      }

      const path = result.rows[0].cloud_file_path;
      const stat = fs.statSync(path);
      const fileSize = stat.size;
      const range = req.headers.range;

      if (range) {
        const parts = range.replace(/bytes=/, "").split("-");
        const start = parseInt(parts[0], 10);
        const end = parts[1] ? parseInt(parts[1], 10) : fileSize-1;
        const chunksize = (end-start)+1;
        const file = fs.createReadStream(path, {start, end});
        const head = {
          'Content-Range': `bytes ${start}-${end}/${fileSize}`,
          'Accept-Ranges': 'bytes',
          'Content-Length': chunksize,
          'Content-Type': `video/${result.rows[0].file_type_shortcut}`
        };
        res.writeHead(206, head);
        file.pipe(res);
      } else {
        const head = {
          'Content-Length': fileSize,
          'Content-Type': `video/${result.rows[0].file_type_shortcut}`
        };
        res.writeHead(200, head);
        fs.createReadStream(path).pipe(res);
      }
    });
  });
});

router.get('/new', (req, res) => {
  const year = `${req}-01-01 00:00:00.000000`;

  pool.connect((err, client, done) => {
    if(err){
      console.log(database.logConnectionError(err));
      res.status(400).send(err);
    }
    client.query(database.postNewFilm(), [
      film, note, film_state,
      year, add_time, by_user,
      path, size, file_state,
      type], (err, result) => {
      done();
      if(err){
        console.log(err);
        res.status(400).send(err);
      }
      res.status(200).send(result.rows);
    });
  });
});

/* --------------------------------------------------- */

module.exports = router;