const isYear = (year) => {
  return Math.sign(year) === 1;
};

module.exports = {
  isYear
};
