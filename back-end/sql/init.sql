CREATE TABLE IF NOT EXISTS film
(
  id                      SERIAL       NOT NULL
    CONSTRAINT films_pkey
    PRIMARY KEY,
  film_name               VARCHAR(256) NOT NULL,
  film_note               VARCHAR(1024),
  state_id                INTEGER      NOT NULL,
  film_year               TIMESTAMP    NOT NULL,
  film_time_of_add        TIMESTAMP    NOT NULL,
  film_time_of_change     TIMESTAMP,
  film_added_by_user_id   INTEGER      NOT NULL,
  film_changed_by_user_id INTEGER,
  film_next_film_id       INTEGER
    CONSTRAINT film_film_id_fk
    REFERENCES film,
  film_prev_film_id       INTEGER
    CONSTRAINT bfilm_bfilm_id_fk
    REFERENCES film
);

CREATE UNIQUE INDEX IF NOT EXISTS films_film_name_uindex
  ON film (film_name);

CREATE UNIQUE INDEX IF NOT EXISTS film_film_next_film_id_uindex
  ON film (film_next_film_id);

CREATE UNIQUE INDEX IF NOT EXISTS film_film_prev_film_id_uindex
  ON film (film_prev_film_id);

CREATE TABLE IF NOT EXISTS cloud_file
(
  id                            SERIAL       NOT NULL
    CONSTRAINT file_path_pkey
    PRIMARY KEY,
  cloud_file_path               VARCHAR(512) NOT NULL,
  cloud_file_size               INTEGER      NOT NULL,
  state_id                      INTEGER      NOT NULL,
  file_type_id                  INTEGER      NOT NULL,
  cloud_file_time_of_add        TIMESTAMP    NOT NULL,
  cloud_file_time_of_change     TIMESTAMP,
  "cloud_file_added_by user_id" INTEGER      NOT NULL,
  cloud_file_changed_by_user_id INTEGER
);

CREATE UNIQUE INDEX IF NOT EXISTS file_path_file_path_uindex
  ON cloud_file (cloud_file_path);

CREATE TABLE IF NOT EXISTS "user"
(
  id              SERIAL       NOT NULL
    CONSTRAINT user_pkey
    PRIMARY KEY,
  user_name       VARCHAR(256) NOT NULL,
  user_first_name VARCHAR(256) NOT NULL,
  user_last_name  VARCHAR(256) NOT NULL,
  role_id         INTEGER      NOT NULL,
  user_note       VARCHAR(1024),
  state_id        INTEGER      NOT NULL,
  user_password   VARCHAR(256) NOT NULL
);

CREATE UNIQUE INDEX IF NOT EXISTS user_user_name_uindex
  ON "user" (user_name);

ALTER TABLE film
  ADD CONSTRAINT film_user_id_fk
FOREIGN KEY (film_added_by_user_id) REFERENCES "user";

CREATE TABLE IF NOT EXISTS role
(
  id        SERIAL       NOT NULL
    CONSTRAINT role_pkey
    PRIMARY KEY,
  role_name VARCHAR(256) NOT NULL,
  role_note VARCHAR(1024),
  state_id  INTEGER      NOT NULL
);

CREATE UNIQUE INDEX IF NOT EXISTS role_role_name_uindex
  ON role (role_name);

ALTER TABLE "user"
  ADD CONSTRAINT user_role_id_fk
FOREIGN KEY (role_id) REFERENCES role;

CREATE TABLE IF NOT EXISTS process
(
  id           SERIAL       NOT NULL
    CONSTRAINT process_pkey
    PRIMARY KEY,
  process_name VARCHAR(256) NOT NULL,
  process_note VARCHAR(1024),
  state_id     INTEGER      NOT NULL
);

CREATE UNIQUE INDEX IF NOT EXISTS process_process_name_uindex
  ON process (process_name);

CREATE TABLE IF NOT EXISTS role_process
(
  id         SERIAL  NOT NULL
    CONSTRAINT role_process_id_pk
    PRIMARY KEY,
  role_id    INTEGER NOT NULL
    CONSTRAINT role_process_role_id_fk
    REFERENCES role,
  process_id INTEGER NOT NULL
    CONSTRAINT role_process_process_id_fk
    REFERENCES process
);

CREATE TABLE IF NOT EXISTS film_cloud_file
(
  id            SERIAL  NOT NULL
    CONSTRAINT film_file_path_id_pk
    PRIMARY KEY,
  film_id       INTEGER NOT NULL
    CONSTRAINT film_file_path_film_id_fk
    REFERENCES film,
  cloud_file_id INTEGER NOT NULL
    CONSTRAINT film_file_path_file_path_id_fk
    REFERENCES cloud_file
);

CREATE UNIQUE INDEX IF NOT EXISTS film_cloud_file_cloud_file_id_uindex
  ON film_cloud_file (cloud_file_id);

CREATE TABLE IF NOT EXISTS state
(
  id         SERIAL       NOT NULL
    CONSTRAINT state_pkey
    PRIMARY KEY,
  state_name VARCHAR(256) NOT NULL,
  state_note VARCHAR(1024)
);

CREATE UNIQUE INDEX IF NOT EXISTS state_state_name_uindex
  ON state (state_name);

ALTER TABLE film
  ADD CONSTRAINT film_state_id_fk
FOREIGN KEY (state_id) REFERENCES state;

ALTER TABLE cloud_file
  ADD CONSTRAINT cloud_file_state_id_fk
FOREIGN KEY (state_id) REFERENCES state;

ALTER TABLE "user"
  ADD CONSTRAINT user_state_id_fk
FOREIGN KEY (state_id) REFERENCES state;

ALTER TABLE role
  ADD CONSTRAINT role_state_id_fk
FOREIGN KEY (state_id) REFERENCES state;

ALTER TABLE process
  ADD CONSTRAINT process_state_id_fk
FOREIGN KEY (state_id) REFERENCES state;

CREATE TABLE IF NOT EXISTS file_type
(
  id                 SERIAL       NOT NULL
    CONSTRAINT file_type_pkey
    PRIMARY KEY,
  file_type_name     VARCHAR(256) NOT NULL,
  file_type_shortcut VARCHAR(32)  NOT NULL,
  file_type_note     VARCHAR(1024)
);

CREATE UNIQUE INDEX IF NOT EXISTS file_type_file_type_name_uindex
  ON file_type (file_type_name);

CREATE UNIQUE INDEX IF NOT EXISTS file_type_file_type_shortcut_uindex
  ON file_type (file_type_shortcut);

ALTER TABLE cloud_file
  ADD CONSTRAINT cloud_file_file_type_id_fk
FOREIGN KEY (file_type_id) REFERENCES file_type;

CREATE TABLE IF NOT EXISTS film_tag_film
(
  id          SERIAL  NOT NULL
    CONSTRAINT film_tag_film_pkey
    PRIMARY KEY,
  film_tag_id INTEGER NOT NULL,
  film_id     INTEGER NOT NULL
    CONSTRAINT film_tag_film_film_id_fk
    REFERENCES film
);

CREATE TABLE IF NOT EXISTS film_tag
(
  id                          SERIAL       NOT NULL
    CONSTRAINT film_tag_pkey
    PRIMARY KEY,
  film_tag_name               VARCHAR(256) NOT NULL,
  film_tag_note               VARCHAR(1024),
  film_tag_time_of_add        TIMESTAMP    NOT NULL,
  film_tag_time_of_change     TIMESTAMP,
  film_tag_added_by_user_id   INTEGER      NOT NULL,
  film_tag_changed_by_user_id INTEGER
);

CREATE UNIQUE INDEX IF NOT EXISTS film_tag_film_tag_name_uindex
  ON film_tag (film_tag_name);

ALTER TABLE film_tag_film
  ADD CONSTRAINT film_tag_film_film_tag_id_fk
FOREIGN KEY (film_tag_id) REFERENCES film_tag;

CREATE TABLE IF NOT EXISTS genre
(
  id         SERIAL       NOT NULL
    CONSTRAINT genre_pkey
    PRIMARY KEY,
  genre_name VARCHAR(256) NOT NULL,
  genre_note VARCHAR(1024)
);

CREATE UNIQUE INDEX IF NOT EXISTS genre_genre_name_uindex
  ON genre (genre_name);

CREATE TABLE IF NOT EXISTS film_genre
(
  id       SERIAL  NOT NULL
    CONSTRAINT film_genre_pkey
    PRIMARY KEY,
  film_id  INTEGER NOT NULL
    CONSTRAINT film_genre_film_id_fk
    REFERENCES film,
  genre_id INTEGER NOT NULL
    CONSTRAINT film_genre_genre_id_fk
    REFERENCES genre
);


