/**
 * Model for find film form
 */
export class FindFilmForm {

  /**
   * Constructor of Find Film Model
   * @param {string} name
   * @param {number} year
   */
  constructor(public name?: string,
              public year?: number) {}
}
