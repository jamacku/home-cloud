/**
 * Model for new film form
 */
export class NewFilmForm {

  /**
   * Constructor of New Film Model
   * @param {string} name
   * @param {string} note
   * @param {number} year
   * @param {number} prev
   * @param {number} next
   * @param {boolean} filmState
   * @param {string} path
   * @param {string} file
   * @param {boolean} cloudState
   */
  constructor(public name: string,
              public note: string,
              public year: number,
              public prev: number,
              public next: number,
              public filmState: boolean,
              public path: string,
              public file: string,
              public cloudState: boolean) {}
}
