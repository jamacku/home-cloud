import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

import { Film } from '../../../film.model';

@Component({
  selector: 'app-list-of-videos-item',
  templateUrl: './list-of-videos-item.component.html',
  styleUrls: ['./list-of-videos-item.component.css']
})
export class ListOfVideosItemComponent {
  @Input('filmData') film: Film;
  bookmark: boolean;

  constructor(private router: Router) { }

  onClickBookmark() {
    this.bookmark = !this.bookmark;
    // TODO Send query to database
  }

  onClickPlay() {
    this.router.navigate(['/films', this.film.id]);
  }
}
