import { Component, OnInit } from '@angular/core';

import { Film } from '../../film.model';
import { FilmService } from '../../film.service';
import { ActivatedRoute, Params } from '@angular/router';
import { FindFilmForm } from '../../../shared/findFilmForm.model';

@Component({
  selector: 'app-list-of-videos',
  templateUrl: './list-of-videos.component.html',
  styleUrls: ['./list-of-videos.component.css']
})
export class ListOfVideosComponent implements OnInit {
  films: Film[] = [];

  constructor(private filmService: FilmService,
              private route: ActivatedRoute) {}

  ngOnInit() {
    this.route.queryParams.subscribe(
      (queryParams: Params) => {
        console.log(queryParams);
        this.filmService.getFilms(new FindFilmForm(queryParams['name'], +queryParams['year']));
      }
    );
    this.filmService.filmsChanged
      .subscribe(
        (films: Film[]) => {
          this.films = films;
        }
      );
  }
}
