import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-new-film-form',
  templateUrl: './new-film-form.component.html',
  styleUrls: ['./new-film-form.component.css']
})
export class NewFilmFormComponent implements OnInit {
  newFilmForm: FormGroup;

  constructor() { }

  ngOnInit() {
    this.newFilmForm = new FormGroup({
      'name': new FormControl(null),
      'note': new FormControl(null),
      'year': new FormControl(null, Validators.min(1920)),
      'prev': new FormControl(null),
      'next': new FormControl(null),
      'f-state': new FormControl(true),
      'path': new FormControl(null),
      'file': new FormControl(null),
      'c-state': new FormControl(true)
    });
  }

  onSubmit() {
    console.log(this.newFilmForm);
    // this.router.navigate(['/films', 'find', 'list'], { queryParams: this.newFilmForm.value});
  }
}
