import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { VgAPI } from 'videogular2/core';

import { Film } from '../film.model';
import { CloudService } from '../../cloud.service';
import { FilmService } from '../film.service';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.css']
})
export class VideoComponent implements OnInit {
  film: Film;
  filmSource = new Array<{
    url: string,
    fileType: string,
  }>();
  subtitlesOptions: {
    url: string,
    label: string,
    lang: string
  };
  dropdownState = false;

  // Videogular
  preload = 'auto';
  videogularApi: VgAPI;

  constructor(private cloudService: CloudService,
              private filmService: FilmService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    // TODO Not a best solution because it's now related on first visit /films to preload array of films :(
    this.route.params.subscribe(
      (params: Params) => {
        this.dropdownState = false;                             // hide dropdown menu
        this.film = this.filmService.getFilm(+params['id']);
        console.log(this.film);
        if (this.film === undefined) {
          this.router.navigate(['/films/find']);
        } else {
          this.filmSource.push({
            url: this.cloudService.getFilmUrl(this.film.id),
            fileType: `video/${this.film.fileType}`
          });
        }
      }
    );
  }

  /**
   * Init of Vg API
   * @param {VgAPI} api
   */
  onPlayerReady(api: VgAPI): void { this.videogularApi = api; }

  onClickHome() {
    // TODO Maybe save watched film?
    this.router.navigate(['/films', 'find']);
  }

  onClickAction() {
    this.dropdownState = !this.dropdownState;
  }

  onClickCopy() {
    // TODO https://stackoverflow.com/questions/36328159/how-do-i-copy-to-clipboard-in-angular-2-typescript
  }

  onClickBookmark() {
    // TODO Bookmark video
  }

  onClickAddTag() {
    return;
  }

  /**
   * Method for Prev and Next button
   * Pouse video and set time to 0 and navigete to new route with prev/next video
   * @param {number} filmId - id of prev/next video
   */
  onClickNextPrev(filmId: number) {
    this.videogularApi.pause();
    this.videogularApi.getDefaultMedia().currentTime = 0;
    this.router.navigate(['/films', filmId]);
  }
}
